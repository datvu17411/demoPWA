import firebase from 'firebase'

var config = {
  apiKey: 'AIzaSyD1UbI3WQlJa6WCrG5EnGuAYZlOrzM1Kvg',
  authDomain: 'cropchat-72dc0.firebaseapp.com',
  databaseURL: 'https://cropchat-72dc0.firebaseio.com',
  storageBucket: 'cropchat-72dc0.appspot.com',
  messagingSenderId: '669303234851'
}
firebase.initializeApp(config)

export default {
  database: firebase.database()
}
